#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <json-c/json.h>

#include "tgbot-api-c/bot.h"
#include "tgbot-api-c/types.h"
#include "tgbot-api-c/config.h"
#include "tgbot-api-c/debug.h"

#define APFREE_WIFIDOG_CMD  "/usr/bin/wdctlx "
#define APFREE_CMD_OUT_LENGTH   128

#define APFREE_BOT_USAGE    "/status \n"    \
"/reset ip|mac \n"  \
"/stop \n"  \
"/add_trusted_domains domain1,domain2...\n"  \
"/clear_trusted_domains \n" \
"/show_trusted_domains \n"

json_object *j_cmd_process = NULL;

static void
apfree_bot_send_help(struct bot_request_context *context, int64_t chat_id)
{
    struct message_config *msg_config = new_message_config(chat_id, APFREE_BOT_USAGE);
    if (!msg_config) return;

    bot_send_message(context, msg_config);
    clean_message_config(msg_config);
}

static void
apfree_bot_process_command(struct bot_request_context *context, struct message *msg)
{
    char *cmd = get_message_command(msg);
    if (!cmd) {
        if (msg->chat)
            apfree_bot_send_help(context, msg->chat->id);
        return;
    } 

    int cmd_len = strlen(cmd)+strlen(APFREE_WIFIDOG_CMD)+8;
    char *apfree_cmd = calloc(1, cmd_len);
    snprintf(apfree_cmd, cmd_len, "%s%s 2>&1", APFREE_WIFIDOG_CMD, cmd);
    
    printf("cmd [%s] \n", apfree_cmd);
    

    struct message_config *msg_config;
    FILE *fp = popen(apfree_cmd, "r");
    if (!fp) {
        msg_config = new_message_config(msg->chat->id, "popen failed");
    } else {
        char *buff = calloc(1, APFREE_CMD_OUT_LENGTH);
        char c_out, *p_out = buff;
        int len = 0;
        while((c_out = fgetc(fp)) != EOF) {
            p_out[len] = c_out;
            len++;
            if ((len%APFREE_CMD_OUT_LENGTH) == 0) {
                buff = realloc(buff, 2*len);
            }
        }
        p_out[len] = '\0';
        pclose(fp);
        msg_config = new_message_config(msg->chat->id, buff);
        free(buff);
    }
        
    if (!msg_config) {
        free(cmd);
        free(apfree_cmd);
        return;
    }

    bot_send_message(context, msg_config);

    free(cmd);
    free(apfree_cmd);
    clean_message_config(msg_config);
}

static void
apfree_bot_process_update(struct bot_request_context *context, void *args)
{
    struct update *item = args;
    assert(item != NULL);

    if (is_message_command(item->message))
        apfree_bot_process_command(context, item->message);
}

static char *progname;

static void
usage()
{
    printf("%s -t bot_token [-i fetch_interval] [-d loglevel] [-f]\n", progname);
}

int 
main(int argc, char **argv)
{
    char *token = NULL;
    int opt, interval = 2, log_level = 7, is_deamon = 1;
    progname = argv[0];
    while ((opt = getopt(argc, argv, "ht:i:d:f")) != -1) {
        switch (opt) {
        case 't':
            token = optarg;
            break;
        case 'i':
            if (optarg)
                interval = atoi(optarg);
            break;
        case 'd':
            if (optarg)
                debugconf.debuglevel = atoi(optarg);
            break;
        case 'f':
            is_deamon = 0;
            break;
        case 'h':
        default:
            usage();
            exit(EXIT_FAILURE);
        }
    }

    if (!token) {
        usage();
        exit(EXIT_FAILURE);
    }

    debug(LOG_DEBUG, "token is %s", token);

    if (is_deamon) {
        daemon(1, 0);
    }

    struct update_config *config = new_update_config(0);
    config->timeout = 60;
    
    bot_get_updates_chan(interval, apfree_bot_process_update, config, token);
}