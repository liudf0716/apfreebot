
#include <event2/event.h>
#include <event2/event_struct.h>

#include "bot.h"
#include "debug.h"

static struct json_object *decode_api_response_to_json_object(struct evhttp_request *req);

typedef void (*cb_bot_api_res_process)(struct evhttp_request *req, void *args);

struct update_head g_updates;

/**
 * @brief process getupdates responses
 * 
 */ 
static void
cb_bot_get_updates_proc(struct evhttp_request *req, void *args)
{
    struct bot_request_context *context = args;
    json_object *j_api_res = decode_api_response_to_json_object(req);
    if (!j_api_res) return;

    json_object *j_result = json_object_object_get(j_api_res, "result");
    if (!j_result) goto ERR;

    struct update_head *list = bot_decode_result_to_update_list(j_result);
    if (!list) goto ERR;

    struct update *item;
    while((item = update_dequeue(list))) {
        debug(LOG_DEBUG, "item is %p", item);
        struct update_config *config = context->config;
        if (item->update_id >= config->offset)
            config->offset = item->update_id +1; 
        update_queue_head(&g_updates, item);
    }
    free(list);

ERR:
    json_object_put(j_api_res);
}

/**
 * @brief process send message response
 */ 
static void
cb_bot_send_message_proc(struct evhttp_request *req, void *args)
{
    json_object *j_api_res = decode_api_response_to_json_object(req);
    if (!j_api_res) return;

    debug(LOG_DEBUG,  "cb_bot_send_message_proc");
}

/**
 * 
 */
static void
cb_bot_default_proc(struct evhttp_request *req, void *args) 
{
    json_object *j_api_res = decode_api_response_to_json_object(req);
    if (!j_api_res) return;

    debug(LOG_DEBUG, "cb_bot_default_proc  \n");
}

struct bot_cmd_process {
    char *cmd;
    cb_bot_api_res_process bot_api_res_process;
} bot_cmd_proc[] = {
    { BOT_GET_UPDATES, cb_bot_get_updates_proc },
    { BOT_SEND_MESSAGE, cb_bot_send_message_proc },
    { NULL, NULL },
};

/**
 * @brief according to bot_cmd, get its api_response process callback func
 * 
 */ 
cb_bot_api_res_process 
bot_get_process_cb(char *bot_cmd)
{
    int i = 0; 
    while(bot_cmd_proc[i].cmd) {
        if (strcmp(bot_cmd_proc[i].cmd, bot_cmd) == 0)
            return bot_cmd_proc[i].bot_api_res_process;
        i++;
    }

    return NULL;
}


static void 
openssl_init()
{
#if (OPENSSL_VERSION_NUMBER < 0x10100000L) || \
	(defined(LIBRESSL_VERSION_NUMBER) && LIBRESSL_VERSION_NUMBER < 0x20700000L)
	// Initialize OpenSSL
	SSL_library_init();
	ERR_load_crypto_strings();
	SSL_load_error_strings();
	OpenSSL_add_all_algorithms();
#endif

    if (!RAND_poll()) exit(EXIT_FAILURE);
}

/**
 * @brief new bot_request_context, need to be free by caller
 * 
 * @return fail NULL 
 */ 
struct bot_request_context *
bot_request_context_new(struct event_base *base, SSL *ssl)
{
    struct bot_request_context *context = calloc(1, sizeof(struct bot_request_context));
    if (!context) {
        return NULL;
    }

    struct bufferevent *bev = bufferevent_openssl_socket_new(base, -1, ssl,
			BUFFEREVENT_SSL_CONNECTING, BEV_OPT_CLOSE_ON_FREE|BEV_OPT_DEFER_CALLBACKS);
    if (!bev) {
        free(context);
        return NULL;
    } 
    
    bufferevent_openssl_set_allow_dirty_shutdown(bev, 1);

    context->base 	= base;
	context->ssl 	= ssl;
	context->bev	= bev;

	return context; 
}

/**
 * @intern
 * @brief read api response to bot_mem_list, need to be free by caller
 *  
 */
static char *
read_api_response(struct evhttp_request *req)
{
    struct evbuffer *in = evhttp_request_get_input_buffer(req);
    size_t len = evbuffer_get_length(in);
    char *res = calloc(1, len+1);
    if (res) {
        memcpy(res, evbuffer_pullup(in, len), len);
    }
    evbuffer_drain(in, len);

    return res;
}

/**
 * @brief decode bot api's result to update list
 * 
 */
struct update_head *
bot_decode_result_to_update_list(json_object *j_result)
{
    if (json_object_get_type(j_result) != json_type_array)
        return NULL;

    struct update_head *list = decode_json_object_to_update_list(j_result);
    return list;
} 


/**
 * @intern
 * @brief decode tg robot api response 
 * 
 */
static struct json_object *
decode_api_response_to_json_object(struct evhttp_request *req)
{
    char *linear_data = read_api_response(req);
    if (!linear_data) return NULL;

    debug(LOG_DEBUG, "response :[%s]\n", linear_data);

    json_object *json_res = json_tokener_parse(linear_data);
    if (!json_res) goto ERR;

    free(linear_data);
ERR:
    return json_res;
}

/**
 * @brief bot send message to bot server
 */ 
void
bot_send_message(struct bot_request_context *context, struct message_config *config)
{
    char *value = message_config_value(config);
    if (!value) return;

    bot_make_request(context, BOT_SEND_MESSAGE, value, BOT_API_TYPE);
    free(value);
}

/**
 * @brief according to end_point, get request uri
 */ 
char *
bot_get_request_uri(char *end_point, char *token, enum bot_request_type type)
{
#define BOT_URI_LENGTH  256
    char buff[BOT_URI_LENGTH] = {0};  
    switch (type) {
    case BOT_API_TYPE:
        snprintf(buff, BOT_URI_LENGTH, "/bot%s/%s", token, end_point);
        break;
    case BOT_FILE_TYPE:
        snprintf(buff, BOT_URI_LENGTH, "/file/bot%s/%s", token, end_point);
    } 
    
    return strndup(buff, strlen(buff));
}

/**
 * @brief bot make request to tg bot server
 */ 
void
bot_make_request(struct bot_request_context *context, char *endpoint, char *param, enum bot_request_type type)
{
    cb_bot_api_res_process cb_func = bot_get_process_cb(endpoint);
    if (!cb_func) {
        cb_func = cb_bot_default_proc;
    }

    struct evhttp_connection *evcon = evhttp_connection_base_bufferevent_new(
        context->base, NULL, context->bev, 
        TG_BOT_API_HOST, TG_BOT_API_PORT);
    if (!evcon) return;

    evhttp_connection_set_timeout(evcon, BOT_CONNECT_TIMEOUT);

    struct evhttp_request *req = evhttp_request_new(cb_func, context);
    if (!req) {
        evhttp_connection_free(evcon);
        return;
    }

    evhttp_add_header(evhttp_request_get_output_headers(req), "Host", TG_BOT_API_HOST);
    evhttp_add_header(evhttp_request_get_output_headers(req), "Connection", "close");
    evhttp_add_header(evhttp_request_get_output_headers(req), "User-Agent", "ApFree");
    if (param) {
        char length[8] = {0};
        snprintf(length, 8, "%u", strlen(param));
        evhttp_add_header(evhttp_request_get_output_headers(req), "Content-Type", "application/x-www-form-urlencoded");
        evhttp_add_header(evhttp_request_get_output_headers(req), "Content-Length", length);
        evbuffer_add_printf(evhttp_request_get_output_buffer(req), "%s", param);
    }
    

    char *uri = bot_get_request_uri(endpoint, context->token, type);
    evhttp_make_request(evcon, req, EVHTTP_REQ_POST, uri);
    free(uri);
}

/**
 * @brief getUpdates callback for timer
 */ 
static void
cb_process_updates(evutil_socket_t fd, short event, void *args)
{
    struct bot_request_context *context = (struct bot_request_context *)args;
    if (!context) return; // impossible here

    char *value = update_config_value(context->config);
    bot_make_request(context, BOT_GET_UPDATES, value, BOT_API_TYPE);

    if (g_updates.qlen == 0) return;

    debug(LOG_DEBUG, "process get update [%u]\n", g_updates.qlen);

    struct update *item;
    while((item = update_dequeue(&g_updates))) {
        if (context->bot_request_process)
            context->bot_request_process(context, item);
        clean_update(item);
    }
}

/**
 * @brief starts and returns a channel for getting updates.
 */ 
void 
bot_get_updates_chan(uint32_t interval, bot_request_process cb, void *config, char *token) 
{
    struct event evtimer;

    update_head_init(&g_updates);

    openssl_init();

	/* Create a new OpenSSL context */
	SSL_CTX *ssl_ctx = SSL_CTX_new(SSLv23_method());
	if (!ssl_ctx) exit(EXIT_FAILURE);

	struct event_base *base = event_base_new();
	if (!base) exit(EXIT_FAILURE);

    SSL *ssl = SSL_new(ssl_ctx);
	if (!ssl) exit(EXIT_FAILURE);
    
	struct bot_request_context *context = bot_request_context_new(base, ssl);
	if (!context) exit(EXIT_FAILURE);

	struct timeval tv;
    context->bot_request_process = cb;
    context->config = config;
    context->token = token;
	event_assign(&evtimer, base, -1, EV_PERSIST, cb_process_updates, (void*)context);
	evutil_timerclear(&tv);
	tv.tv_sec = interval?interval:1;
    event_add(&evtimer, &tv);

	event_base_dispatch(base);
}