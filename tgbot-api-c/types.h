#ifndef _TYPES_
#define _TYPES_

#include <json-c/json.h>
#include "common.h"

// ResponseParameters are various errors that can be returned in APIResponse.
struct response_parameters {
    int64_t migrate_to_chat_id; // optional
    int retry_after; // optional
};

// GroupChat is a group chat.
struct group_chat {
    int id;
    char *title;
};

// ChatPhoto represents a chat photo.
struct chat_photo {
    char *small_file_id;
    char *big_file_id;
};

// Chat contains information about the place a message was sent.
struct chat {
    int64_t id;
    char *type; 
    char *title; // optional
    char *username; // optional
    char *first_name; // optional
    char *last_name; // optional
    uint32_t all_members_are_administrators; // optional
    struct chat_photo *photo;
    char *description; // optional
    char *invite_link; // optional
};

struct chat_photo *decode_json_object_to_chat_photo(json_object *);

// User is a user on Telegram.
struct user {
    struct user *next;
    struct user *prev;
    int id;
    char *first_name;
    char *last_name;
    char *username;
    char *language_code;
    int is_bot;
};

struct user_head;
struct user *decode_json_object_to_user(json_object *);
struct user_head *decode_json_object_to_user_list(json_object *);

// Location contains information about a place.
struct location {
    double longitude;
    double latitude;
};

// Contact contains information about a contact.
//
// Note that LastName and UserID may be empty.
struct contact {
    char *phone_number;
    char *first_name;
    char *last_name; // optional
    int user_id; // optional
};

// File contains information about a file to download from Telegram.
struct file {
    char *file_id;
    int file_size;
    char *file_path;
};

// WebhookInfo is information about a currently set webhook.
struct web_hook_info {
    char *url;
    int has_custom_certificate;
    int pending_update_count;
    int last_error_date;
    char *last_error_message;
};

// InputMediaVideo contains a video for displaying as part of a media group.
struct input_media_video {
    char *type;
    char *media;
    char *caption;
    char *parse_mode;
    int width;
    int height;
    int duration;
    int supports_streaming;
};

// PhotoSize contains information about photos.
struct photo_size {
    struct photo_size *next;
    struct photo_size *prev;
    char *file_id;
    int width;
    int height;
    int file_size; // optional
};
struct photo_size_head;
struct photo_size *decode_json_object_to_photo_size(json_object *);
struct photo_size_head *decode_json_object_to_photo_size_list(json_object *);

// UserProfilePhotos contains a set of user profile photos.
struct user_profile_photos {
    int total_count;
    struct photo_size **photos;
};

// Audio contains information about audio.
struct audio {
    char *file_id;
    int duration;
    char *performer; //optional
    char *title; // optional
    char *mime_type; // optional
    int file_size; // optional
};

struct audio *decode_json_object_to_audio(json_object *j_audio);

// Document contains information about a document.
struct document {
    char *file_id;
    struct photo_size *thumb; // optional
    char *file_name; // optional
    char *mime_type; // optional
    int file_size; // optional
};

struct document *decode_json_object_to_document(json_object *j_document);

// Sticker contains information about a sticker.
struct sticker {
    char *file_id;
    int width;
    int height;
    struct photo_size *thumb; // optional
    char *emoji; // optional
    int file_size; // optional
    char *set_name; // optional
};

struct sticker *decode_json_object_to_sticker(json_object *j_sticker);

// ChatAnimation contains information about an animation.
struct chat_animation {
    char *file_id;
    int width;
    int height;
    int duration;
    struct photo_size *thumb; // optional
    char *file_name; // optional
    char *mime_type; // optional
    int file_size; // optional
};

struct chat_animation *decode_json_object_to_chat_animation(json_object *);

// Video contains information about a video.
struct video {
    char *file_id;
    int width;
    int height;
    int duration;
    struct photo_size *thumb; // optional
    char *mime_type; // optional
    int file_size; // optional
};

struct video *decode_json_object_to_video(json_object *);

// VideoNote contains information about a video.
struct video_note {
    char *file_id;
    int length;
    int duration;
    struct photo_size *thumb; // optional
    int file_size; // optional
};

struct video_note *decode_json_object_to_video_note(json_object *);

// Voice contains information about a voice.
struct voice {
    char *file_id;
    int duration;
    char *mime_type; // optional
    int file_size; // optional
};

struct voice *decode_json_object_to_voice(json_object *);

// Venue contains information about a venue, including its Location.
struct venue {
    struct location *location;
    char *title;
    char *address;
    char *foursquare_id; // optional
};

// KeyboardButton is a button within a custom keyboard.
struct keyboard_button {
    char *text;
    int request_contact;
    int request_location;
};

// ReplyKeyboardHide allows the Bot to hide a custom keyboard.
struct reply_keyboard_hide {
    int hide_keyboard;
    int selective; // optional
};

// ReplyKeyboardRemove allows the Bot to hide a custom keyboard.
struct reply_keyboard_remove {
    int remove_keyboard;
    int selective;
};

// ReplyKeyboardMarkup allows the Bot to set a custom keyboard.
struct reply_keyboard_markup {
    struct keyboard_button **keyboard;
    int resize_keyboard; // optional
    int one_time_keyboard; // optional
    int selective; // optional
};

// InlineKeyboardButton is a button within a custom keyboard for
// inline query responses.
//
// Note that some values are references as even an empty string
// will change behavior.
//
// CallbackGame, if set, MUST be first button in first row.
struct inline_keyboard_button {
    char *text;
    char *url; // optional
    char *callback_data; // optional
    char *switch_inline_query; // optional
    struct callback_game *switch_inline_query_current_chat; // optional
    char *callback_game; // optional
    int pay; // optional
};

// CallbackQuery is data sent when a keyboard button with callback data
// is clicked.
struct callback_query {
    char *id;
    struct user *from;
    struct message *message; // optional
    char *inline_message_id; // optional
    char *chat_instance;
    char *data; // optional
    char *game_short_name; // optional
};

struct callback_query *decode_json_object_to_callback_query(json_object *);

// InlineKeyboardMarkup is a custom keyboard presented for an inline bot.
struct inline_keyboard_markup {
    struct inline_keyboard_button ** inline_keyboard;
};

// InlineQueryResultArticle is an inline query response article.
struct inline_query_result_article {
    char *type; // required
    char *id; // required
    char *title; // required
    void *input_message_content; // required
    char *url;
    int hide_url;
    char *description;
    char *thumb_url;
    char *thumb_width;
    char *thumb_height;
};

// InlineQueryResultPhoto is an inline query response photo.
struct inline_query_result_photo {
    char *type; // required
    char *id; // required
    char *photo_url; // required
    char *mime_type;
    int photo_width;
    int photo_height;
    char *thumb_url;
    char *title;
    char *description;
    char *caption;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultGIF is an inline query response GIF.
struct inline_query_result_gif {
    char *type; // required
    char *id; // required
    char *gif_url; // required
    int gif_width;
    int gif_height;
    int gif_duration;
    char *thumb_url;
    char *title;
    char *caption;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultMPEG4GIF is an inline query response MPEG4 GIF.
struct inline_query_result_mpeg4_gif {
    char *type; // required
    char *id; // required
    char *mpeg4_url; // required
    int mpeg4_width;
    int mpeg4_height;
    int mpeg4_duration;
    char *thumb_url;
    char *title;
    char *caption;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultVideo is an inline query response video.
struct inline_query_result_video {
    char *type; // required
    char *id; // required
    char *video_url; // required
    char *mime_type; // required
    char *thumb_url;
    char *title;
    char *caption;
    int video_width;
    int video_height;
    int video_duration;
    char *description;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultAudio is an inline query response audio.
struct inline_query_result_audio {
    char *type; // required
    char *id; // required
    char *audio_url; // required
    char *title; // required
    char *caption;
    char *performer;
    int audio_duration;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultVoice is an inline query response voice.
struct inline_query_result_voice {
    char *type; // required
    char *id; // required
    char *title; // required
    char *caption;
    int voice_duration;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultDocument is an inline query response document.
struct inline_query_result_document {
    char *type; // required
    char *id; // required
    char *title; // required
    char *caption;
    char *document_url; // required
    char *mime_type; // required
    char *description;
    char *thumb_url;
    int thumb_width;
    int thumb_height;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};
// InlineQueryResultLocation is an inline query response location.
struct inline_query_result_location {
    char *type; // required
    char *id; // required
    float latitude; // required
    float longitude; // required
    char *title; // required
    char *thumb_url;
    int thumb_width;
    int thumb_height;
    struct inline_keyboard_markup *reply_markup;
    void *input_message_content;
};

// InlineQueryResultGame is an inline query response game.
struct inline_query_result_game {
    char *type;
    char *id;
    char *game_short_name;
    struct inline_keyboard_markup *reply_markup;
};

// ChosenInlineResult is an inline query result chosen by a User
struct chosen_inline_result {
    char *result_id;
    struct user *from;
    struct location *location;
    char *inline_message_id;
    char *query;
};

// InputTextMessageContent contains text for displaying
// as an inline query result.
struct input_text_message_content {
    char *message_text;
    char *parse_mode;
    int disable_web_page_preview;
};

// InputLocationMessageContent contains a location for displaying
// as an inline query result.
struct input_location_message_content {
    float latitude;
    float longitude;
};

// InputVenueMessageContent contains a venue for displaying
// as an inline query result.
struct input_venue_message_content {
    float latitude;
    float longitude;
    char *title;
    char *address;
    char *foursquare_id;
};

// InputContactMessageContent contains a contact for displaying
// as an inline query result.
struct input_contact_message_content {
    char *phone_number;
    char *first_name;
    char *last_name;
};

// Invoice contains basic information about an invoice.
struct invoice {
    char *title;
    char *description;
    char *start_parameter;
    char *currency;
    int total_amount;
};

// LabeledPrice represents a portion of the price for goods or services.
struct label_price {
    char *label;
    int amount;
};

// ShippingAddress represents a shipping address.
struct shipping_address {
    char *country_code;
    char *state;
    char *city;
    char *street_line1;
    char *street_line2;
    char *post_code;
};

struct shipping_address *decode_json_object_to_shipping_address(json_object *);

// OrderInfo represents information about an order.
struct order_info {
    char *name;
    char *phone_number;
    char *email;
    struct shipping_address *shipping_address;
};

struct order_info *decode_json_object_to_order_info(json_object *);

// ShippingOption represents one shipping option.
struct shipping_option {
    char *id;
    char *title;
    struct labeled_price **prices;
};

// SuccessfulPayment contains basic information about a successful payment.
struct successful_payment {
    char *currency;
    int total_amount;
    char *invoice_payload;
    char *shipping_option_id;
    struct order_info *order_info;
    char *telegram_payment_charge_id;
    char *provider_payment_charge_id;
};

// ShippingQuery contains information about an incoming shipping query.
struct shipping_query {
    char *id;
    struct user *from;
    char *invoice_payload;
    struct shipping_address *shipping_address;
};

// PreCheckoutQuery contains information about an incoming pre-checkout query.
struct pre_checkout_query {
    char *id;
    struct user *from;
    char *currency;
    int total_amount;
    char *invoice_payload;
    char *shipping_option_id;
    struct order_info *order_info;
};

struct pre_checkout_query *decode_json_object_to_pre_checkout_query(json_object *);

// ForceReply allows the Bot to have users directly reply to it without
// additional interaction.
struct force_reply {
    int force_reply;
    int selective; // optional
};

// ChatMember is information about a member in a chat.
struct chat_member {
    struct user *user;
    char *status;
    int64_t until_date; // optional
    int can_be_edited; // optional
    int can_change_info; // optional
    int can_post_messages; // optional
    int can_edit_messages; // optional
    int can_delete_messages; // optional
    int can_restrict_members; // optional
    int can_promote_members; // optional
    int can_send_messages; // optional
    int can_send_media_messages; // optional
    int can_send_other_messages; // optional
    int can_add_web_page_previews; // optional
};

// MessageEntity contains information about data in a Message.
struct message_entity {
    struct message_entity *next;
    struct message_entity *prev;
    char *type;
    int offset;
    int length;
    char *url; // optional
    struct user *user; // optional
};
struct message_entity_head;
struct message_entity *decode_json_object_to_message_entity(json_object *j_entity);
struct message_entity_head *decode_json_object_to_message_entity_list(json_object *j_entity_list);

// Animation is a GIF animation demonstrating the game.
struct animation {
    char *file_id;
    struct photo_size *thumb;
    char *file_name;
    char *mime_type;
    int file_size;
};

struct animation *decode_json_object_to_animation(json_object *);

// GameHighScore is a user's score and position on the leaderboard.
struct game_high_score {
    int position;
    struct user *user;
    int score;
};

// Game is a game within Telegram.
struct game {
    char *title;
    char *description;
    struct photo_size_head *photo;
    char *text;
    struct message_entity_head *text_entities;
    struct animation *animation;
};

struct game *decode_json_object_to_game(json_object *);

// APIResponse is a response from the Telegram API with the result
// stored raw.
struct api_response {
    int ok;
    json_object *result; // json result
    int error_code;
    char *description;
    struct response_parameters *parameters;
};

struct api_response *decode_json_object_to_api_response(json_object *);

void clean_api_response(struct api_response *);

// InlineQuery is a Query from Telegram for an inline request.
struct inline_query {
    char *id;
    char *query;
    char *offset;
    struct user *user;
    struct location *location;
};

struct inline_query *decode_json_object_to_inline_query(json_object *);

// Message is returned by almost every request, and contains data about
// almost anything.
struct message {
    int message_id;
    struct user *from; // optional
    int date;
    struct chat *chat; 
    struct user *forward_from; // optional
    struct chat *forward_from_chat; // optional
    int forward_from_message_id; // optional
    int forward_date; // optional
    struct message *reply_to_message; // optional
    int edit_date; // optional
    char *text; // optional
    struct message_entity_head *entities; // optional
    struct audio *audio; // optional
    struct document *document; // optional
    struct chat_animation *animation; // optional
    struct game *game; // optional
    struct photo_size_head *photo; // optional
    struct sticker *sticker; // optional
    struct video *video; // optional
    struct video_note *video_note; // optional
    struct voice *voice; // optional
    char *caption; // optional
    struct contact *contact; // optional
    struct location *location; // optional
    struct venue *venue; // optional
    struct user_head *new_chat_members; // optional
    struct user *left_chat_member; // optional
    char *new_chat_title; // optional
    struct photo_size_head *new_chat_photo; // optional
    int delete_chat_photo; // optional
    int group_chat_created; // optional
    int supergroup_chat_created; // optional
    int channel_chat_created; // optional
    int64_t migrate_to_chat_id; // optional
    int64_t migrate_from_chat_id; // optional
    struct message *pinned_message; // optional
    struct invoice *invoice; // optional
    struct successful_payment *successful_payment; // optional
    // struct passport_data *passport_data; // optional
};

int is_message_command(struct message *);
char *get_message_command(struct message *);

// Update is an update response, from GetUpdates.
struct update {
    struct update *next;
    struct update *prev;
    int update_id;
    struct message *message;
    struct message *edited_message;
    struct message *channel_post;
    struct message *edited_channel_post;
    struct inline_query *inline_query;
    struct chosen_inline_result *chosen_inline_result;
    struct callback_query *callback_query;
    struct shipping_query *shipping_query;
    struct pre_checkout_query *pre_checkout_query;
};

struct update_head;
struct update_head *decode_json_object_to_update_list(json_object *);
struct update *decode_json_object_to_update(json_object *);
struct message *decode_json_object_to_message(json_object *);

void clean_update(struct update *);

struct contact *decode_json_object_to_contact(json_object *);
struct location *decode_json_object_to_location(json_object *);
struct venue *decode_json_object_to_venue(json_object *);
struct invoice *decode_json_object_to_invoice(json_object *);
struct successful_payment *decode_json_object_to_successful_payment(json_object *);
struct chat *decode_json_object_to_chat(json_object *);
struct chosen_inline_result *decode_json_object_to_chosen_inline_result(json_object *);
struct shipping_query *decode_json_object_to_shipping_query(json_object *);
struct response_parameters *decode_json_object_to_response_parameters(json_object *);

DECLARE_LIST(user)
DECLARE_LIST_INIT(user)
DECLARE_LIST_ADD_HEAD(user, new_user)
DECLARE_LIST_DEL_HEAD(user)

DECLARE_LIST(photo_size)
DECLARE_LIST_INIT(photo_size)
DECLARE_LIST_ADD_HEAD(photo_size, new_photo_size)
DECLARE_LIST_DEL_HEAD(photo_size)

DECLARE_LIST(update)
DECLARE_LIST_INIT(update)
DECLARE_LIST_ADD_HEAD(update, new_update)
DECLARE_LIST_DEL_HEAD(update)

DECLARE_LIST(message_entity)
DECLARE_LIST_INIT(message_entity)
DECLARE_LIST_ADD_HEAD(message_entity, new_message_entity)
DECLARE_LIST_DEL_HEAD(message_entity)

#endif