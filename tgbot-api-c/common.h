#ifndef _COMMON_
#define _COMMON_

#include <stdint.h>
#include <string.h>
#include <assert.h>

#include <json-c/json.h>

#define BOT_COMMAND     "bot_command"

#define DECLARE_LIST(name)      \
struct name##_head {            \
    struct name *next;          \
    struct name *prev;          \
    size_t qlen;                \
};                              

#define DECLARE_LIST_INIT(name)                             \
inline void name##_head_init(struct name##_head *list)   \
{                                                   \
    list->prev = list->next = (struct name *)list;  \
    list->qlen = 0;                                 \
};

#define DECLARE_LIST_ADD_HEAD(name, new_item)               \
inline void name##_queue_head(struct name##_head *list, struct name *new_item)   \
{                                                   \
    struct name *prev = (struct name *)list;        \
    struct name *next = prev->next;                 \
    new_item->next = next;                          \
    new_item->prev = prev;                          \
    next->prev = prev->next = new_item;             \
    list->qlen++;                                   \
};

#define DECLARE_LIST_DEL_HEAD(name)                         \
inline struct name* name##_dequeue(struct name##_head *list)  \
{                                                   \
    struct name *item = list->next;                 \
    if (item == (struct name *)list)                \
        return NULL;                                \
    struct name *next, *prev;                       \
    list->qlen--;                       \
    next = item->next;                  \
    prev = item->prev;                  \
    item->next = item->prev = NULL;     \
    next->prev = prev;                  \
    prev->next = next;                  \
    return item;                        \
};

#define JSON_GET_OPTIONAL_VALUE(obj, name, decode_func)  \
    json_object *j_##name = json_object_object_get(j_##obj, #name); \
    if (j_##name) obj->name = decode_func(j_##name);

#define JSON_GET_VALUE(obj, name, decode_func)  \
    json_object *j_##name = json_object_object_get(j_##obj, #name); \
    assert(j_##name != NULL);    \
    obj->name = decode_func(j_##name);

#define JSON_GET_OPTIONAL_STRING(obj, name)  \
    json_object *j_##name = json_object_object_get(j_##obj, #name); \
    if (j_##name) obj->name = strdup(json_object_get_string(j_##name));

#define JSON_GET_STRING(obj, name)  \
    json_object *j_##name = json_object_object_get(j_##obj, #name); \
    assert(j_##name != NULL);   \
    obj->name = strdup(json_object_get_string(j_##name));

#define DECODE_DECLARE(name)    \
    assert(j_##name != NULL);   \
    struct name *name = calloc(1, sizeof(struct name));  \
    if (!name) return NULL;

#define DECODE_JSON_ARRAY_TO_OBJ_LIST(name, j_array_obj, decode_item_func)                \
    assert(json_object_get_type(j_array_obj) == json_type_array);          \
    struct name##_head *list = calloc(1, sizeof(struct name##_head));   \
    if (!list) return NULL;                                             \
                                                                        \
    name##_head_init(list);                                             \
    for (int i= 0; i < json_object_array_length(j_array_obj); i++) {       \
        json_object *j_item = json_object_array_get_idx(j_array_obj, i);   \
        struct name *new_item = decode_item_func(j_item);               \
        if (new_item)                                                   \
            name##_queue_head(list, new_item);                          \
    }                                                                   \
    if (list->qlen) return list;                                        \
    else {                                                              \
        free(list);                                                     \
        return NULL;                                                    \
    }

#endif