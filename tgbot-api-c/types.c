#include <stdio.h>

#include "types.h"
#include "debug.h"

/**
 * @brief decode json_object to message_entity struct
 */
struct message_entity *
decode_json_object_to_message_entity(json_object *j_message_entity)
{
    DECODE_DECLARE(message_entity)

    JSON_GET_STRING(message_entity, type)
    JSON_GET_VALUE(message_entity, offset, json_object_get_int)
    JSON_GET_VALUE(message_entity, length, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(message_entity, url)
    JSON_GET_OPTIONAL_VALUE(message_entity, user, decode_json_object_to_user)

    return message_entity;
}

/**
 * @brief decode json_object to photo_size struct
 */ 
struct photo_size *
decode_json_object_to_photo_size(json_object *j_photo_size)
{
    DECODE_DECLARE(photo_size)

    JSON_GET_STRING(photo_size, file_id)
    JSON_GET_VALUE(photo_size, width, json_object_get_int)
    JSON_GET_VALUE(photo_size, height, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(photo_size, file_size, json_object_get_int)

    return photo_size;
}

/**
 * @brief decode json_object to photo_size list
 */ 
struct photo_size_head *
decode_json_object_to_photo_size_list(json_object *j_photo_size_list)
{
    DECODE_JSON_ARRAY_TO_OBJ_LIST(photo_size, j_photo_size_list, decode_json_object_to_photo_size)
}

/**
 * @brief decode json_object to document struct
 */ 
struct document *
decode_json_object_to_document(json_object *j_document)
{
    DECODE_DECLARE(document)

    JSON_GET_STRING(document, file_id)
    JSON_GET_VALUE(document, thumb, decode_json_object_to_photo_size)
    JSON_GET_OPTIONAL_STRING(document, file_name)
    JSON_GET_OPTIONAL_STRING(document, mime_type)
    JSON_GET_OPTIONAL_VALUE(document, file_size, json_object_get_int)

    return document;
}


/**
 * @brief decode json_object to sticker struct
 * 
 */
struct sticker *
decode_json_object_to_sticker(json_object *j_sticker)
{
    DECODE_DECLARE(sticker)

    JSON_GET_STRING(sticker, file_id)
    JSON_GET_VALUE(sticker, width, json_object_get_int)
    JSON_GET_VALUE(sticker, height, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(sticker, emoji)
    JSON_GET_OPTIONAL_VALUE(sticker, file_size, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(sticker, set_name)
    JSON_GET_OPTIONAL_VALUE(sticker, thumb, decode_json_object_to_photo_size)

    return sticker;
}

/**
 * @brief decode json_object to audio struct
 * 
 */ 
struct audio *
decode_json_object_to_audio(json_object *j_audio)
{
    DECODE_DECLARE(audio)

    JSON_GET_STRING(audio, file_id)
    JSON_GET_VALUE(audio, duration, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(audio, performer)
    JSON_GET_OPTIONAL_STRING(audio, title)
    JSON_GET_OPTIONAL_STRING(audio, mime_type)
    JSON_GET_OPTIONAL_VALUE(audio, file_size, json_object_get_int)

    return audio;
}

/**
 * @brief decode json_object to chat_animation struct
 */
struct chat_animation *
decode_json_object_to_chat_animation(json_object *j_chat_animation)
{
    DECODE_DECLARE(chat_animation)

    JSON_GET_STRING(chat_animation, file_id)
    JSON_GET_VALUE(chat_animation, width, json_object_get_int)
    JSON_GET_VALUE(chat_animation, height, json_object_get_int)
    JSON_GET_VALUE(chat_animation, duration, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(chat_animation, file_name)
    JSON_GET_OPTIONAL_STRING(chat_animation, mime_type)
    JSON_GET_OPTIONAL_VALUE(chat_animation, file_size, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(chat_animation, thumb, decode_json_object_to_photo_size)

    return chat_animation;
} 

/**
 * @brief decode json_object to video struct
 */ 
struct video *
decode_json_object_to_video(json_object *j_video)
{
    DECODE_DECLARE(video)

    JSON_GET_STRING(video, file_id)
    JSON_GET_VALUE(video, width, json_object_get_int)
    JSON_GET_VALUE(video, height, json_object_get_int)
    JSON_GET_VALUE(video, duration, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(video, mime_type)
    JSON_GET_OPTIONAL_VALUE(video, file_size, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(video, thumb, decode_json_object_to_photo_size)

    return video;
}

/**
 * @brief decode json_object to video_note struct
 * 
 */
struct video_note *
decode_json_object_to_video_note(json_object *j_video_note)
{
    DECODE_DECLARE(video_note)

    JSON_GET_STRING(video_note, file_id)
    JSON_GET_VALUE(video_note, length, json_object_get_int)
    JSON_GET_VALUE(video_note, duration, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(video_note, file_size, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(video_note, thumb, decode_json_object_to_photo_size)

    return video_note;
}

/**
 * @brief dcode json_object to voice struct
 * 
 */
struct voice *
decode_json_object_to_voice(json_object *j_voice)
{
    DECODE_DECLARE(voice)

    JSON_GET_STRING(voice, file_id)
    JSON_GET_VALUE(voice, duration, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(voice, mime_type)
    JSON_GET_OPTIONAL_VALUE(voice, file_size, json_object_get_int)

    return voice;
} 

/**
 * @brief decode json_object to contact
 * 
 */
struct contact *
decode_json_object_to_contact(json_object *j_contact)
{
    DECODE_DECLARE(contact)

    JSON_GET_STRING(contact, phone_number)
    JSON_GET_STRING(contact, first_name)
    JSON_GET_OPTIONAL_STRING(contact, last_name)
    JSON_GET_OPTIONAL_VALUE(contact, user_id, json_object_get_int)

    return contact;
} 

/**
 * @brief decode json_object to location
 *  
 */
struct location *
decode_json_object_to_location(json_object *j_location)
{
    DECODE_DECLARE(location)

    JSON_GET_VALUE(location, longitude, json_object_get_double)
    JSON_GET_VALUE(location, latitude, json_object_get_double)

    return location;
}

/**
 * @brief decode json_object to venue
 * 
 */
struct venue *
decode_json_object_to_venue(json_object *j_venue)
{
    DECODE_DECLARE(venue)

    JSON_GET_STRING(venue, title)
    JSON_GET_STRING(venue, address)
    JSON_GET_OPTIONAL_STRING(venue, foursquare_id)

    return venue;
} 

/**
 * 
 */
struct invoice *
decode_json_object_to_invoice(json_object *j_invoice)
{
    DECODE_DECLARE(invoice)

    JSON_GET_STRING(invoice, title)
    JSON_GET_STRING(invoice, description)
    JSON_GET_STRING(invoice, start_parameter)
    JSON_GET_STRING(invoice, currency)
    JSON_GET_VALUE(invoice, total_amount, json_object_get_int)

    return invoice;
} 

/**
 * 
 */
struct successful_payment *
decode_json_object_to_successful_payment(json_object *j_successful_payment) 
{
    DECODE_DECLARE(successful_payment)

    JSON_GET_STRING(successful_payment, currency)
    JSON_GET_VALUE(successful_payment, total_amount, json_object_get_int)
    JSON_GET_STRING(successful_payment, invoice_payload)
    JSON_GET_OPTIONAL_STRING(successful_payment, shipping_option_id)
    JSON_GET_STRING(successful_payment, telegram_payment_charge_id)
    JSON_GET_STRING(successful_payment, provider_payment_charge_id)

    return successful_payment;
}

/**
 * @brief 
 * 
 */
struct message_entity_head *
decode_json_object_to_message_entity_list(json_object *j_entity_list)
{
    DECODE_JSON_ARRAY_TO_OBJ_LIST(message_entity, j_entity_list, decode_json_object_to_message_entity)
} 

/**
 * @brief decode json_object to chat_photo struct
 */ 
struct chat_photo *
decode_json_object_to_chat_photo(json_object *j_chat_photo)
{
    DECODE_DECLARE(chat_photo)

    JSON_GET_STRING(chat_photo, small_file_id)
    JSON_GET_STRING(chat_photo, big_file_id)

    return chat_photo;
}

/**
 * @brief decode json_object to animation struct
 */ 

struct animation *
decode_json_object_to_animation(json_object *j_animation)
{
    DECODE_DECLARE(animation)

    JSON_GET_STRING(animation, file_id)
    JSON_GET_VALUE(animation, thumb, decode_json_object_to_photo_size)
    JSON_GET_STRING(animation, file_name)
    JSON_GET_STRING(animation, mime_type)
    JSON_GET_VALUE(animation, file_size, json_object_get_int)

    return animation;
}

/**
 * @brief decode json_object to game struct
 */ 
struct game *
decode_json_object_to_game(json_object *j_game)
{
    DECODE_DECLARE(game)

    JSON_GET_STRING(game, title)
    JSON_GET_STRING(game, description)
    JSON_GET_VALUE(game, photo, decode_json_object_to_photo_size_list)
    JSON_GET_STRING(game, text)
    JSON_GET_VALUE(game, text_entities, decode_json_object_to_message_entity_list)
    JSON_GET_VALUE(game, animation, decode_json_object_to_animation)

    return game;
}

/**
 * @brief decode json_object to chat struct
 * 
 */ 
struct chat *
decode_json_object_to_chat(json_object *j_chat)
{
    DECODE_DECLARE(chat)

    JSON_GET_VALUE(chat, id, json_object_get_int64)
    JSON_GET_STRING(chat, type);
    JSON_GET_OPTIONAL_STRING(chat, title)
    JSON_GET_OPTIONAL_STRING(chat, username)
    JSON_GET_OPTIONAL_STRING(chat, first_name)
    JSON_GET_OPTIONAL_STRING(chat, last_name)
    JSON_GET_OPTIONAL_VALUE(chat, all_members_are_administrators, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(chat, description)
    JSON_GET_OPTIONAL_STRING(chat, invite_link)
    JSON_GET_OPTIONAL_VALUE(chat, photo, decode_json_object_to_chat_photo)

    return chat;
}

/**
 * @brief decode json_object to user struct
 */
struct user *
decode_json_object_to_user(json_object *j_user)
{
    DECODE_DECLARE(user)

     JSON_GET_VALUE(user, id, json_object_get_int)
     JSON_GET_STRING(user, first_name)
     JSON_GET_OPTIONAL_STRING(user, last_name)
     JSON_GET_OPTIONAL_STRING(user, username)
     JSON_GET_OPTIONAL_STRING(user, language_code)
     JSON_GET_OPTIONAL_VALUE(user, is_bot, json_object_get_boolean)

     return user;
 }

/**
 * @brief decode json_object to user_head struct
 */ 
struct user_head *
decode_json_object_to_user_list(json_object *j_user_list)
{
    DECODE_JSON_ARRAY_TO_OBJ_LIST(user, j_user_list, decode_json_object_to_user)
}

/**
 * @brief decode json_object to message structure
 * 
 */ 
struct message *
decode_json_object_to_message(json_object *j_message)
{
    DECODE_DECLARE(message)

    JSON_GET_VALUE(message, message_id, json_object_get_int)
    JSON_GET_VALUE(message, date, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(message, from , decode_json_object_to_user)
    JSON_GET_VALUE(message, chat, decode_json_object_to_chat)
    JSON_GET_OPTIONAL_VALUE(message, forward_from, decode_json_object_to_user)
    JSON_GET_OPTIONAL_VALUE(message, forward_from_chat, decode_json_object_to_chat)
    JSON_GET_OPTIONAL_VALUE(message, forward_from_message_id, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(message, forward_date, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(message, reply_to_message, decode_json_object_to_message)
    JSON_GET_OPTIONAL_VALUE(message, edit_date, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(message, text)
    JSON_GET_OPTIONAL_VALUE(message, entities, decode_json_object_to_message_entity_list)
    JSON_GET_OPTIONAL_VALUE(message, audio, decode_json_object_to_audio)
    JSON_GET_OPTIONAL_VALUE(message, document, decode_json_object_to_document)
    JSON_GET_OPTIONAL_VALUE(message, animation, decode_json_object_to_chat_animation)
    JSON_GET_OPTIONAL_VALUE(message, game, decode_json_object_to_game)
    JSON_GET_OPTIONAL_VALUE(message, photo, decode_json_object_to_photo_size_list)
    JSON_GET_OPTIONAL_VALUE(message, sticker, decode_json_object_to_sticker)
    JSON_GET_OPTIONAL_VALUE(message, video, decode_json_object_to_video)
    JSON_GET_OPTIONAL_VALUE(message, video_note, decode_json_object_to_video_note)
    JSON_GET_OPTIONAL_VALUE(message, voice, decode_json_object_to_voice)
    JSON_GET_OPTIONAL_STRING(message, caption)
    JSON_GET_OPTIONAL_VALUE(message, contact, decode_json_object_to_contact)
    JSON_GET_OPTIONAL_VALUE(message, location, decode_json_object_to_location)
    JSON_GET_OPTIONAL_VALUE(message, venue, decode_json_object_to_venue)
    JSON_GET_OPTIONAL_VALUE(message, new_chat_members, decode_json_object_to_user_list)
    JSON_GET_OPTIONAL_VALUE(message, left_chat_member, decode_json_object_to_user)
    JSON_GET_OPTIONAL_STRING(message, new_chat_title)
    JSON_GET_OPTIONAL_VALUE(message, new_chat_photo, decode_json_object_to_photo_size_list)
    JSON_GET_OPTIONAL_VALUE(message, delete_chat_photo, json_object_get_boolean)
    JSON_GET_OPTIONAL_VALUE(message, group_chat_created, json_object_get_boolean)
    JSON_GET_OPTIONAL_VALUE(message, supergroup_chat_created, json_object_get_boolean)
    JSON_GET_OPTIONAL_VALUE(message, channel_chat_created, json_object_get_boolean)
    JSON_GET_OPTIONAL_VALUE(message, migrate_to_chat_id, json_object_get_int64)
    JSON_GET_OPTIONAL_VALUE(message, migrate_from_chat_id, json_object_get_int64)
    JSON_GET_OPTIONAL_VALUE(message, pinned_message, decode_json_object_to_message)
    JSON_GET_OPTIONAL_VALUE(message, invoice, decode_json_object_to_invoice)
    JSON_GET_OPTIONAL_VALUE(message, successful_payment, decode_json_object_to_successful_payment)

    return message;
}

/**
 * @brief decode json_object to inline_query
 */ 

struct inline_query *
decode_json_object_to_inline_query(json_object *j_inline_query)
{
    DECODE_DECLARE(inline_query)

    JSON_GET_STRING(inline_query, id)
    JSON_GET_VALUE(inline_query, user, decode_json_object_to_user)
    JSON_GET_OPTIONAL_VALUE(inline_query, location, decode_json_object_to_location)
    JSON_GET_STRING(inline_query, query)
    JSON_GET_STRING(inline_query, offset)

    return inline_query;
}

/**
 * @brief decode json_object to chosen_inline_result struct
 */ 
struct chosen_inline_result *
decode_json_object_to_chosen_inline_result(struct json_object *j_chosen_inline_result)
{
    DECODE_DECLARE(chosen_inline_result)

    JSON_GET_STRING(chosen_inline_result, result_id)
    JSON_GET_VALUE(chosen_inline_result, from, decode_json_object_to_user)
    JSON_GET_VALUE(chosen_inline_result, location, decode_json_object_to_location)
    JSON_GET_STRING(chosen_inline_result, inline_message_id)
    JSON_GET_STRING(chosen_inline_result, query)

    return chosen_inline_result;
}

/**
 * @brief 
 */
struct callback_query *
decode_json_object_to_callback_query(json_object *j_callback_query)
{
    DECODE_DECLARE(callback_query)

    JSON_GET_STRING(callback_query, id)
    JSON_GET_VALUE(callback_query, from, decode_json_object_to_user)
    JSON_GET_STRING(callback_query, chat_instance)
    JSON_GET_OPTIONAL_VALUE(callback_query, message, decode_json_object_to_message)
    JSON_GET_OPTIONAL_STRING(callback_query, inline_message_id)
    JSON_GET_OPTIONAL_STRING(callback_query, data)
    JSON_GET_OPTIONAL_STRING(callback_query, game_short_name)

    return callback_query;
}

struct shipping_address *
decode_json_object_to_shipping_address(json_object *j_shipping_address)
{
    DECODE_DECLARE(shipping_address)

    JSON_GET_STRING(shipping_address, country_code)
    JSON_GET_STRING(shipping_address, state)
    JSON_GET_STRING(shipping_address, city)
    JSON_GET_STRING(shipping_address, street_line1)
    JSON_GET_STRING(shipping_address, street_line2)
    JSON_GET_STRING(shipping_address, post_code)

    return shipping_address;
}

/**
 * @brief
 */ 
struct shipping_query *
decode_json_object_to_shipping_query(json_object *j_shipping_query)
{
    DECODE_DECLARE(shipping_query)

    JSON_GET_STRING(shipping_query, id)
    JSON_GET_VALUE(shipping_query, from, decode_json_object_to_user)
    JSON_GET_STRING(shipping_query, invoice_payload)
    JSON_GET_VALUE(shipping_query, shipping_address, decode_json_object_to_shipping_address)

    return shipping_query;
}

/**
 * @brief
 */ 
struct order_info *
decode_json_object_to_order_info(json_object *j_order_info)
{
    DECODE_DECLARE(order_info)

    JSON_GET_OPTIONAL_STRING(order_info, name)
    JSON_GET_OPTIONAL_STRING(order_info, phone_number)
    JSON_GET_OPTIONAL_STRING(order_info, email)
    JSON_GET_OPTIONAL_VALUE(order_info, shipping_address, decode_json_object_to_shipping_address)

    return order_info;
}
/**
 * @brief
 */ 
struct pre_checkout_query *
decode_json_object_to_pre_checkout_query(json_object *j_pre_checkout_query)
{
    DECODE_DECLARE(pre_checkout_query)

    JSON_GET_STRING(pre_checkout_query, id)
    JSON_GET_VALUE(pre_checkout_query, from, decode_json_object_to_user)
    JSON_GET_STRING(pre_checkout_query, currency)
    JSON_GET_VALUE(pre_checkout_query, total_amount, json_object_get_int)
    JSON_GET_STRING(pre_checkout_query, invoice_payload)
    JSON_GET_OPTIONAL_STRING(pre_checkout_query, shipping_option_id)
    JSON_GET_OPTIONAL_VALUE(pre_checkout_query, order_info, decode_json_object_to_order_info)

    return pre_checkout_query;
}

/**
 * @brief decode json_object to update structure
 */ 
struct update *
decode_json_object_to_update(json_object *j_update)
{
    DECODE_DECLARE(update)

    JSON_GET_VALUE(update, update_id, json_object_get_int)
    JSON_GET_OPTIONAL_VALUE(update, message, decode_json_object_to_message)
    JSON_GET_OPTIONAL_VALUE(update, edited_message, decode_json_object_to_message)
    JSON_GET_OPTIONAL_VALUE(update, channel_post, decode_json_object_to_message)
    JSON_GET_OPTIONAL_VALUE(update, edited_channel_post, decode_json_object_to_message)
    JSON_GET_OPTIONAL_VALUE(update, inline_query, decode_json_object_to_inline_query)
    JSON_GET_OPTIONAL_VALUE(update, chosen_inline_result, decode_json_object_to_chosen_inline_result)
    JSON_GET_OPTIONAL_VALUE(update, callback_query, decode_json_object_to_callback_query)
    JSON_GET_OPTIONAL_VALUE(update, shipping_query, decode_json_object_to_shipping_query)
    JSON_GET_OPTIONAL_VALUE(update, pre_checkout_query, decode_json_object_to_pre_checkout_query)

    return update;
}

/**
 * @brief decode json_object to update list
 * 
 */ 
struct update_head *
decode_json_object_to_update_list(json_object *j_update_list)
{
    DECODE_JSON_ARRAY_TO_OBJ_LIST(update, j_update_list, decode_json_object_to_update)
}

/**
 * @brief
 */ 
struct response_parameters *
decode_json_object_to_response_parameters(json_object *j_response_parameters)
{
    DECODE_DECLARE(response_parameters)

    JSON_GET_OPTIONAL_VALUE(response_parameters, migrate_to_chat_id, json_object_get_int64)
    JSON_GET_OPTIONAL_VALUE(response_parameters, retry_after, json_object_get_int)

    return response_parameters;
}

/**
 * @brief
 */ 
struct api_response *
decode_json_object_to_api_response(json_object *j_api_response)
{
    DECODE_DECLARE(api_response)

    JSON_GET_VALUE(api_response, ok, json_object_get_boolean)
    JSON_GET_OPTIONAL_VALUE(api_response, error_code, json_object_get_int)
    JSON_GET_OPTIONAL_STRING(api_response, description)
    JSON_GET_OPTIONAL_VALUE(api_response, parameters, decode_json_object_to_response_parameters)

    api_response->result = json_object_object_get(j_api_response, "result");

    return api_response;
}

/**
 * @brief
 */

static void
pre_checkout_query_free(struct pre_checkout_query *item)
{
    if (!item) return;
    free(item);
}

static void
shipping_query_free(struct shipping_query *item)
{
    if (!item) return;
    free(item);
}

static void
callback_query_free(struct callback_query *item)
{
    if (!item) return;
    free(item);
}

static void
chosen_inline_result_free(struct chosen_inline_result *item)
{
    if (!item) return;
    free(item);
}

static void
inline_query_free(struct inline_query *item)
{
    if (!item) return;

    free(item);
}

static void
message_free(struct message *item)
{
    if (!item) return;

    free(item);
} 

/**
 * @brief
 */ 
void
clean_update(struct update *item)
{
    if (!item) return;

    message_free(item->message);
    message_free(item->edited_message);
    message_free(item->channel_post);
    message_free(item->edited_channel_post);
    inline_query_free(item->inline_query);
    chosen_inline_result_free(item->chosen_inline_result);
    callback_query_free(item->callback_query);
    shipping_query_free(item->shipping_query);
    pre_checkout_query_free(item->pre_checkout_query);

    free(item);
}

static void
response_parameters_free(struct response_parameters *response_parameters)
{
    if (!response_parameters) return;
    free(response_parameters);
}

void 
clean_api_response(struct api_response *api_res)
{
    if (!api_res) return;
    if (api_res->description) free(api_res->description);
    response_parameters_free(api_res->parameters);
    if (api_res->result) json_object_put(api_res->result);

    free(api_res);
}

/**
 * @return 1 true or 0 false
 */
int
is_message_command(struct message *msg)
{
    if (!msg) return 0;

    if (!msg->entities || msg->entities->qlen == 0)
        return 0;

    struct message_entity *entity = msg->entities->next;
    if (entity->offset == 0 && strcmp(entity->type, BOT_COMMAND) == 0)
        return 1;
    else
        return 0;
} 

/**
 * @brief get command
 * @return msg's command or NULL if failed
 * return value need to be free by caller
 */ 
char *
get_message_command(struct message *msg)
{
    if (!is_message_command(msg))
        return NULL;

    char *user_text = msg->text;
    assert(user_text != NULL && user_text[0] == '/');
    char *p = strrchr(user_text, '@');
    if (p)
        return strndup(user_text+1, p-user_text-1);
    else
        return strdup(user_text+1);
}