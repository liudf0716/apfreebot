#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "debug.h"

/**
 * @brief new message_config, need to be free by caller
 * fail return NULL
 * 
 */ 
struct message_config *
new_message_config(int64_t chat_id, char *text)
{
    struct message_config *msg = calloc(1, sizeof(struct message_config));
    if (!msg) return NULL;

    msg->chat.chat_id = chat_id;
    msg->text = strdup(text);
    msg->disable_webpage_preview = 0;

    return msg;
}

/**
 * @intern
 * @brief 
 */ 
static void
base_chat_free(struct base_chat *chat)
{
    if (chat->channel_username) free(chat->channel_username);
    if (chat->reply_markup) free(chat->reply_markup);
}

/**
 * 
 */
void
clean_message_config(struct message_config *msg)
{
    if (!msg) return;

    if (msg->text) free(msg->text);
    base_chat_free(&msg->chat);

    free(msg);
} 

/**
 * @brief parse struct message_config to http post params 
 * @return NULL if failed, else value need to be freed by caller
 */ 
char *
message_config_value(struct message_config *config)
{
    if (!config) return NULL;
    char value[MSG_LENGTH] = {0};
    snprintf(value, MSG_LENGTH, "chat_id=%lld&text=%s", 
        config->chat.chat_id, config->text);

    return strdup(value);
}

/**
 * @brief new update_config 
 */ 
struct update_config *
new_update_config(int offset)
{
    struct update_config *update = calloc(1, sizeof(struct update_config));
    if (!update) return NULL;

    update->offset = offset;
    return update;
}

/**
 * 
 */
char *
update_config_value(struct update_config *config)
{
#define UPDATE_CONFIG_LEN   256
    char value[UPDATE_CONFIG_LEN] = {0};
    int len = 0;
    if (config->offset != 0)
        len = snprintf(value, UPDATE_CONFIG_LEN, "offset=%d", config->offset);
    
    if (config->limit > 0)
        snprintf(value+len, UPDATE_CONFIG_LEN-len, len?"&limit=%d":"limit=%d", config->limit);

    len = strlen(value);
    if (config->timeout > 0)
        snprintf(value+len, UPDATE_CONFIG_LEN-len, len?"&timeout=%d":"timeout=%d", config->timeout);

    return len?strdup(value):NULL;
} 