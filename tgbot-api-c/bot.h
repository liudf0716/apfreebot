#ifndef _BOT_
#define _BOT_

#include <event2/bufferevent_ssl.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <event2/http.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#include "types.h"
#include "config.h"
#include "common.h"

#define BOT_CONNECT_TIMEOUT 10

#define TG_BOT_API_PORT     443
#define TG_BOT_API_HOST     "telegram.wificoin.club"

#define BOT_GET_UPDATES     "getUpdates"
#define BOT_SEND_MESSAGE    "sendMessage"

#define READ_BLOB_SIZE  256

enum bot_request_type 
{
    BOT_API_TYPE,
    BOT_FILE_TYPE
};

struct bot_request_context;

typedef void (*bot_request_process)(struct bot_request_context *, void *); 

struct bot_request_context
{
    struct event_base *base;
    struct bufferevent *bev;
    SSL *ssl; 
    void (*bot_request_process)(struct bot_request_context *, void *);
    void *config;
    char *token;
};

char *bot_get_request_uri(char *, char *, enum bot_request_type);

void bot_get_updates_chan(uint32_t, bot_request_process cb, void *, char *);

void bot_make_request(struct bot_request_context *, char *, char *, enum bot_request_type);

void bot_send_message(struct bot_request_context *, struct message_config *);

struct update_head *bot_decode_result_to_update_list(json_object *);

struct bot_request_context *bot_request_context_new(struct event_base *, SSL *);

#endif