#ifndef _CONFIG_
#define _CONFIG_

#include <stdint.h>

#define MSG_LENGTH  1024
struct base_chat {
    int64_t chat_id;
    char *channel_username;
    int reply_to_message_id;
    char *reply_markup;
    int disable_notification;
};

struct message_config {
    struct base_chat chat;
    char *text;
    char *parse_mode;
    int disable_webpage_preview;
};

struct user_profile_photos_config {
    int user_id;
    int offset;
    int limit;
};

struct update_config {
    int offset;
    int limit;
    int timeout;
};

struct update_config *new_update_config(int);

char *update_config_value(struct update_config *);

struct message_config *new_message_config(int64_t chat_id, char *text);

char *message_config_value(struct message_config *config);

void clean_message_config(struct message_config *);

#endif